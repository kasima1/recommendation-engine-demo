async def contain_based_recommandation(self, reels_name: str):
    def get_title_from_index(index):
        return df[df.index == index]["title"].values[0]

    def get_id_from_title(title):
        return df[df.title == title]["id"].values[0]

    # conn = psycopg2.connect(database=config.POSTGRES_DB, user=config.POSTGRES_USER, password=config.POSTGRES_PASSWORD,
    #                         host=config.POSTGRES_SERVER, port=config.POSTGRES_PORT)

    conn = psycopg2.connect(database="yt_stories", user="kamyab", password="bhala", host="localhost", port="5432")

    # df = pd.read_csv("movie_recommender/movie_dataset.csv")
    # df = pd.read_sql('SELECT * FROM post ORDER BY updated_at DESC;', conn)
    df = pd.read_sql('SELECT * FROM post;', conn)
    # features = ['keywords', 'cast', 'genres', 'director']
    features = ['tags']

    for feature in features:
        df[feature] = df[feature].fillna('')

    def combine_features(row):
        try:
            return " ".join(np.array(list(row[features])).flatten())
        except:
            print("Error:", row)

    df["combined_features"] = df.apply(combine_features, axis=1)

    count_matrix = CountVectorizer().fit_transform(df["combined_features"])

    cosine_sim = cosine_similarity(count_matrix)

    reels_id = get_id_from_title(reels_name)

    reels_index = (df[df['id'] == reels_id].index.values)[0]

    similar_reels = list(enumerate(cosine_sim[reels_index]))

    sorted_similar_movies = sorted(similar_reels, key=lambda x: x[1], reverse=True)

    data = []
    i = 0
    for element in sorted_similar_movies:
        if (cosine_sim[reels_index] >= 0.90).any():
            title = get_title_from_index(element[0])
            result = await self.post_repo.get_by_title(title)
            data.append(result)
            i = i + 1
            if i > 50:
                break
    return data

from faker import Faker
from faker.providers import DynamicProvider

medical_professions_provider = DynamicProvider(
     provider_name="medical_profession",
     elements=["dr.", "doctor", "nurse", "surgeon", "clerk"],
)

fake = Faker()


my_word_list = [
'danish','cheesecake','sugar',
'Lollipop','wafer','Gummies',
'sesame','Jelly','beans',
'pie','bar','Ice','oat' ]

print(fake.random_elements(my_word_list, unique=True))

fake.sentence()
# print(fake.sentence(ext_word_list=my_word_list))

# print(fake.sentence(ext_word_list=my_word_list))

user_list = ['eb76b985-709a-41ec-9cd0-5e80940782dd', '6ce13e3d-49ff-4677-8e14-468b50fed1c9',
             '288b56d1-07fd-41d0-bf44-872626e9b1b6', '88a919c4-0ac9-4ea9-ad78-c3f9e53c434d',
             '626bca5f-f292-4190-90dc-b1a6edcfca18', '8c6f4a8b-2604-4a1f-9a2d-981845eef50d',
             '54d4dfa4-3763-442b-b903-c0f7e06cf752', '67dad3ae-f9dc-4cda-a49b-01d1dcf6f535',
             'a65947b6-8faa-47a5-9084-e4e3090243a7', '9fafdac2-405e-43b3-b575-3c69f7ba03d3',
             'f5561776-a080-4b1c-8686-2c2a061c158f', '7b104137-ec5c-45d9-ae70-057ae7efe5f3'
             ]

print(fake.random_elements(user_list, unique=True, length=1))




# then add new provider to faker instance
fake.add_provider(medical_professions_provider)

# now you can use:
# print(fake.medical_profession())


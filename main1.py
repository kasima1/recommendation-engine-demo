import pandas as pd
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity

rating = pd.read_csv("Collaborative Filtering/dataset/toy_dataset.csv", index_col=0)
rating = rating.fillna(0)

# print(rating)

def standardize(row):
    new_row = (row - row.mean()) / (row.max() - row.min())
    return new_row

rating_std = rating.apply(standardize)

# print(rating_std)

item_similarity = cosine_similarity(rating_std.T)

# print(item_similarity)

item_similarity_df = pd.DataFrame(item_similarity, index=rating.columns, columns=rating.columns)

# print(item_similarity_df)

def get_similar_movies(movie_name, user_rating):
    similar_score = item_similarity_df[movie_name]*(user_rating-2.5)
    similar_score = similar_score.sort_values(ascending=False)

    # print(item_similarity_df[movie_name])
    # print(item_similarity_df[movie_name]*(user_rating-2.5))
    #
    # print({"1": similar_score})

    return similar_score

# print(get_similar_movies("romantic3", 4))

action_lover = [("action1", 5), ("romantic2", 1), ("romantic3", 1)]
# action_lover = [("action1", 5)]

similar_scores = pd.DataFrame()

for movie, rating in action_lover:
    similar_scores = similar_scores.append(get_similar_movies(movie, rating), ignore_index=True)

print(similar_scores.head(10))
# print(similar_scores.sum().sort_values(ascending=False))
print(similar_scores.sum())
print(similar_scores.sum().sort_values(ascending=False))
import numpy as np
import uvicorn
from fastapi import FastAPI
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import psycopg2
import uuid

app = FastAPI()


@app.post("/recommadation")
def contain_based_recommandation(movie_name: str):

    def get_title_from_id(id):
        return df[df.id == id]["title"].values[0]

    def get_id_from_title(title):
        return df[df.title == title]["id"].values[0]

    conn = psycopg2.connect(database="yt_stories", user="kamyab", password="bhala", host="localhost", port="5432")

    # df = pd.read_csv("movie_recommender/movie_dataset.csv")
    # df = pd.read_sql('SELECT * FROM post ORDER BY updated_at DESC;', conn)
    df = pd.read_sql('SELECT * FROM post;', conn)
    print(df)
    # index = df.index
    # for i in index:
    #     print(i)
    # df = df.set_index('id')
    # print(df)




    # features = ['keywords', 'cast', 'genres', 'director']
    features = ['tags']

    for feature in features:
        df[feature] = df[feature].fillna('')

    def combine_features(row):
        try:
            return " ".join(list(row[features])[0])
        except:
            print("Error:", row)


    # convert_uuid_to_int = ['id']
    #
    # for element in convert_uuid_to_int:
    #     df[element] = df[element].int
    #     print(df[element])

    df["combined_features"] = df.apply(combine_features, axis=1)

    cv = CountVectorizer()

    count_matrix = cv.fit_transform(df["combined_features"])

    cosine_sim = cosine_similarity(count_matrix)

    # print(cosine_sim[0])

    movie_user_likes = movie_name

    movie_id = get_id_from_title(movie_user_likes)

    # print((df[df['id'] == movie_id].index.values)[0])

    movie_index = (df[df['id'] == movie_id].index.values)[0]
    # print(movie_index)

    # similar_movies = list(enumerate(cosine_sim[movie_index]))
    similar_movies = list(enumerate(cosine_sim[movie_index]))
    # print(similar_movies)

    def get_title_from_index(index):
        return df[df.index == index]["title"].values[0]

    sorted_similar_movies = sorted(similar_movies, key=lambda x: x[1], reverse=True)

    # print(get_title_from_index(4))
    # print(sorted_similar_movies)


    # original_movie_id = (df[df['index'] == sorted_similar_movies].title.values)[0]
    # print(original_movie_id)


    # data = []
    # i = 0
    # for element in sorted_similar_movies:
    #     # if (cosine_sim[movie_index] >= 0.99).any():
    #         print(get_title_from_id(element[0]))
    #         data.append(get_title_from_id(element[0]))
    #         i = i+1
    #         if i > 30:
    #             break
    # return data

    data = []
    i = 0
    for element in sorted_similar_movies:
        # if (cosine_sim[movie_index] >= 0.99).any():
            print(get_title_from_index(element[0]))
            data.append(get_title_from_index(element[0]))
            i = i+1
            if i > 30:
                break
    return data

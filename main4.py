import pandas as pd

ratings = pd.read_csv('Collaborative Filtering/dataset/ratings.csv')
# print(ratings)
movies = pd.read_csv('Collaborative Filtering/dataset/movies.csv')
# print(movies)
ratings = pd.merge(movies, ratings).drop(['genres', 'timestamp'], axis=1)
# print(ratings.shape)
# print(ratings.head())

userRatings = ratings.pivot_table(index=['userId'], columns=['title'], values='rating')
# print(userRatings.head())
# print("Before: ", userRatings.shape)
userRatings = userRatings.dropna(thresh=10, axis=1).fillna(0, axis=1)
# userRatings.fillna(0, inplace=True)
# print("After: ",userRatings.shape)

corrMatrix = userRatings.corr(method='pearson')
# print(corrMatrix.head(100))

item_similarity_df = userRatings.corr(method='pearson')
# print(item_similarity_df.head(50))


def get_similar(movie_name, user_rating):
    similar_ratings = corrMatrix[movie_name]*(user_rating-2.5)
    similar_ratings = similar_ratings.sort_values(ascending=False)
    #print(type(similar_ratings))
    return similar_ratings


# romantic_lover = [("(500) Days of Summer (2009)", 5), ("Alice in Wonderland (2010)", 3), ("Aliens (1986)", 1),
#                   ("2001: A Space Odyssey (1968)", 2)]

romantic_lover = [("(500) Days of Summer (2009)", 5)]



similar_movies = pd.DataFrame()
# similar_movies = []
# # print(similar_movies)

for movie, rating in romantic_lover:
    similar = get_similar(movie, rating)
    print(similar)
    # similar_movies = similar_movies.append(get_similar(movie, rating), ignore_index=True)
    similar_movies = similar_movies.append(similar, ignore_index=True)

#     # similar_movies = similar_movies.pd.concat(get_similar(movie, rating), ignore_index=True)
#     # similar_movies = pd.concat(similar_movies, get_similar(movie, rating))
#
# print(similar_movies.head())
# print(similar_movies)
# print(similar_movies.rating)
# print(similar_movies.sum().sort_values(ascending=False))




from fastapi import FastAPI
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

app = FastAPI()


@app.post("/recommadation")
def contain_based_recommandation(sorting_on: list, movie_name: str):

    def get_title_from_index(index):
        return df[df.index == index]["title"].values[0]

    def get_index_from_title(title):
        return df[df.title == title]["index"].values[0]


    df = pd.read_csv("movie_recommender/movie_dataset.csv")

    # features = ['keywords', 'cast', 'genres', 'director']
    features = sorting_on

    for feature in features:
        df[feature] = df[feature].fillna('')

    def combine_features(row):
        try:
            # return (sum(row[(features)]))
            # print({"1": row['keywords'] + " " + row['cast'] + " " + row["genres"] + " " + row["director"]})
            # print({"2": (sum(row[(features)]))})
            # print({"2": (' '.join(row[(features)]))})
            # return row['keywords']+" "+row['cast']+" "+row["genres"]+" "+row["director"]
            return ' '.join(row[(features)])
        except:
            print("Error:", row)

    df["combined_features"] = df.apply(combine_features, axis=1)

    cv = CountVectorizer()

    count_matrix = cv.fit_transform(df["combined_features"])

    cosine_sim = cosine_similarity(count_matrix)

    # print(cosine_sim)

    movie_user_likes = movie_name

    movie_index = get_index_from_title(movie_user_likes)

    # print(movie_index)

    similar_movies = list(enumerate(cosine_sim[movie_index]))

    print(list(enumerate(cosine_sim[movie_index])))
    print(cosine_sim[5])

    sorted_similar_movies = sorted(similar_movies, key=lambda x: x[1], reverse=True)

    data = []
    i = 0
    for element in sorted_similar_movies:
        if (cosine_sim[movie_index] >= 0.99).any():
            # print(return get_title_from_index(element[0]))
            data.append(get_title_from_index(element[0]))
            i = i+1
            if i > 30:
                break
    return data
